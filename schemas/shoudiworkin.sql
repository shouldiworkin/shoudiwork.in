-- MySQL dump 10.13  Distrib 5.6.25, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: shoudiworkin
-- ------------------------------------------------------
-- Server version	5.6.25-4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `logo` varchar(100) DEFAULT NULL,
  `website` varchar(200) DEFAULT NULL,
  `country` varchar(4) NOT NULL,
  `city` varchar(100) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `updated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `company_ratio` decimal(10,4) NOT NULL,
  `salary_ratio` decimal(10,4) NOT NULL,
  `management_ratio` decimal(10,4) NOT NULL,
  `enviroment_ratio` decimal(10,4) NOT NULL,
  `hr_ratio` decimal(10,4) NOT NULL,
  `location_ratio` decimal(10,4) NOT NULL,
  `comments_qty` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='Company description';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` VALUES (1,'PerchÃ© l\'erba Ã¨ verde?','perche-l-erba-e-verde-af-t2s','','','af','','2015-10-02 13:57:14','2015-10-05 13:57:23',1.0000,2.0000,3.0000,4.0000,5.0000,6.0000,1,5,1),(2,'asdasd','asdasd-bs-up0',NULL,'','bs','','2015-10-05 16:14:32','2015-10-05 16:14:32',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0,5,0),(5,'Ciao Ciccio','ciao-ciccio-it-fku','logo-ciao-ciccio-it-fku.png','http://www.google.com','it','Roma','2015-10-05 16:20:42','2015-10-05 16:20:42',0.0000,0.0000,0.0000,0.0000,0.0000,0.0000,0,5,0);
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL DEFAULT '',
  `alpha_2` varchar(2) NOT NULL DEFAULT '',
  `alpha_3` varchar(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=250 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Afghanistan','af','afg'),(2,'Aland Islands','ax','ala'),(3,'Albania','al','alb'),(4,'Algeria','dz','dza'),(5,'American Samoa','as','asm'),(6,'Andorra','ad','and'),(7,'Angola','ao','ago'),(8,'Anguilla','ai','aia'),(9,'Antarctica','aq',''),(10,'Antigua and Barbuda','ag','atg'),(11,'Argentina','ar','arg'),(12,'Armenia','am','arm'),(13,'Aruba','aw','abw'),(14,'Australia','au','aus'),(15,'Austria','at','aut'),(16,'Azerbaijan','az','aze'),(17,'Bahamas','bs','bhs'),(18,'Bahrain','bh','bhr'),(19,'Bangladesh','bd','bgd'),(20,'Barbados','bb','brb'),(21,'Belarus','by','blr'),(22,'Belgium','be','bel'),(23,'Belize','bz','blz'),(24,'Benin','bj','ben'),(25,'Bermuda','bm','bmu'),(26,'Bhutan','bt','btn'),(27,'Bolivia, Plurinational State of','bo','bol'),(28,'Bonaire, Sint Eustatius and Saba','bq','bes'),(29,'Bosnia and Herzegovina','ba','bih'),(30,'Botswana','bw','bwa'),(31,'Bouvet Island','bv',''),(32,'Brazil','br','bra'),(33,'British Indian Ocean Territory','io',''),(34,'Brunei Darussalam','bn','brn'),(35,'Bulgaria','bg','bgr'),(36,'Burkina Faso','bf','bfa'),(37,'Burundi','bi','bdi'),(38,'Cambodia','kh','khm'),(39,'Cameroon','cm','cmr'),(40,'Canada','ca','can'),(41,'Cape Verde','cv','cpv'),(42,'Cayman Islands','ky','cym'),(43,'Central African Republic','cf','caf'),(44,'Chad','td','tcd'),(45,'Chile','cl','chl'),(46,'China','cn','chn'),(47,'Christmas Island','cx',''),(48,'Cocos (Keeling) Islands','cc',''),(49,'Colombia','co','col'),(50,'Comoros','km','com'),(51,'Congo','cg','cog'),(52,'Congo, The Democratic Republic of the','cd','cod'),(53,'Cook Islands','ck','cok'),(54,'Costa Rica','cr','cri'),(55,'Cote d\'Ivoire','ci','civ'),(56,'Croatia','hr','hrv'),(57,'Cuba','cu','cub'),(58,'Curacao','cw','cuw'),(59,'Cyprus','cy','cyp'),(60,'Czech Republic','cz','cze'),(61,'Denmark','dk','dnk'),(62,'Djibouti','dj','dji'),(63,'Dominica','dm','dma'),(64,'Dominican Republic','do','dom'),(65,'Ecuador','ec','ecu'),(66,'Egypt','eg','egy'),(67,'El Salvador','sv','slv'),(68,'Equatorial Guinea','gq','gnq'),(69,'Eritrea','er','eri'),(70,'Estonia','ee','est'),(71,'Ethiopia','et','eth'),(72,'Falkland Islands (Malvinas)','fk','flk'),(73,'Faroe Islands','fo','fro'),(74,'Fiji','fj','fji'),(75,'Finland','fi','fin'),(76,'France','fr','fra'),(77,'French Guiana','gf','guf'),(78,'French Polynesia','pf','pyf'),(79,'French Southern Territories','tf',''),(80,'Gabon','ga','gab'),(81,'Gambia','gm','gmb'),(82,'Georgia','ge','geo'),(83,'Germany','de','deu'),(84,'Ghana','gh','gha'),(85,'Gibraltar','gi','gib'),(86,'Greece','gr','grc'),(87,'Greenland','gl','grl'),(88,'Grenada','gd','grd'),(89,'Guadeloupe','gp','glp'),(90,'Guam','gu','gum'),(91,'Guatemala','gt','gtm'),(92,'Guernsey','gg','ggy'),(93,'Guinea','gn','gin'),(94,'Guinea-Bissau','gw','gnb'),(95,'Guyana','gy','guy'),(96,'Haiti','ht','hti'),(97,'Heard Island and McDonald Islands','hm',''),(98,'Holy See (Vatican City State)','va','vat'),(99,'Honduras','hn','hnd'),(100,'Hong Kong','hk','hkg'),(101,'Hungary','hu','hun'),(102,'Iceland','is','isl'),(103,'India','in','ind'),(104,'Indonesia','id','idn'),(105,'Iran, Islamic Republic of','ir','irn'),(106,'Iraq','iq','irq'),(107,'Ireland','ie','irl'),(108,'Isle of Man','im','imn'),(109,'Israel','il','isr'),(110,'Italy','it','ita'),(111,'Jamaica','jm','jam'),(112,'Japan','jp','jpn'),(113,'Jersey','je','jey'),(114,'Jordan','jo','jor'),(115,'Kazakhstan','kz','kaz'),(116,'Kenya','ke','ken'),(117,'Kiribati','ki','kir'),(118,'Korea, Democratic People\'s Republic of','kp','prk'),(119,'Korea, Republic of','kr','kor'),(120,'Kuwait','kw','kwt'),(121,'Kyrgyzstan','kg','kgz'),(122,'Lao People\'s Democratic Republic','la','lao'),(123,'Latvia','lv','lva'),(124,'Lebanon','lb','lbn'),(125,'Lesotho','ls','lso'),(126,'Liberia','lr','lbr'),(127,'Libyan Arab Jamahiriya','ly','lby'),(128,'Liechtenstein','li','lie'),(129,'Lithuania','lt','ltu'),(130,'Luxembourg','lu','lux'),(131,'Macao','mo','mac'),(132,'Macedonia, The former Yugoslav Republic of','mk','mkd'),(133,'Madagascar','mg','mdg'),(134,'Malawi','mw','mwi'),(135,'Malaysia','my','mys'),(136,'Maldives','mv','mdv'),(137,'Mali','ml','mli'),(138,'Malta','mt','mlt'),(139,'Marshall Islands','mh','mhl'),(140,'Martinique','mq','mtq'),(141,'Mauritania','mr','mrt'),(142,'Mauritius','mu','mus'),(143,'Mayotte','yt','myt'),(144,'Mexico','mx','mex'),(145,'Micronesia, Federated States of','fm','fsm'),(146,'Moldova, Republic of','md','mda'),(147,'Monaco','mc','mco'),(148,'Mongolia','mn','mng'),(149,'Montenegro','me','mne'),(150,'Montserrat','ms','msr'),(151,'Morocco','ma','mar'),(152,'Mozambique','mz','moz'),(153,'Myanmar','mm','mmr'),(154,'Namibia','na','nam'),(155,'Nauru','nr','nru'),(156,'Nepal','np','npl'),(157,'Netherlands','nl','nld'),(158,'New Caledonia','nc','ncl'),(159,'New Zealand','nz','nzl'),(160,'Nicaragua','ni','nic'),(161,'Niger','ne','ner'),(162,'Nigeria','ng','nga'),(163,'Niue','nu','niu'),(164,'Norfolk Island','nf','nfk'),(165,'Northern Mariana Islands','mp','mnp'),(166,'Norway','no','nor'),(167,'Oman','om','omn'),(168,'Pakistan','pk','pak'),(169,'Palau','pw','plw'),(170,'Palestinian Territory, Occupied','ps','pse'),(171,'Panama','pa','pan'),(172,'Papua New Guinea','pg','png'),(173,'Paraguay','py','pry'),(174,'Peru','pe','per'),(175,'Philippines','ph','phl'),(176,'Pitcairn','pn','pcn'),(177,'Poland','pl','pol'),(178,'Portugal','pt','prt'),(179,'Puerto Rico','pr','pri'),(180,'Qatar','qa','qat'),(181,'Reunion','re','reu'),(182,'Romania','ro','rou'),(183,'Russian Federation','ru','rus'),(184,'Rwanda','rw','rwa'),(185,'Saint Barthelemy','bl','blm'),(186,'Saint Helena, Ascension and Tristan Da Cunha','sh','shn'),(187,'Saint Kitts and Nevis','kn','kna'),(188,'Saint Lucia','lc','lca'),(189,'Saint Martin (French Part)','mf','maf'),(190,'Saint Pierre and Miquelon','pm','spm'),(191,'Saint Vincent and The Grenadines','vc','vct'),(192,'Samoa','ws','wsm'),(193,'San Marino','sm','smr'),(194,'Sao Tome and Principe','st','stp'),(195,'Saudi Arabia','sa','sau'),(196,'Senegal','sn','sen'),(197,'Serbia','rs','srb'),(198,'Seychelles','sc','syc'),(199,'Sierra Leone','sl','sle'),(200,'Singapore','sg','sgp'),(201,'Sint Maarten (Dutch Part)','sx','sxm'),(202,'Slovakia','sk','svk'),(203,'Slovenia','si','svn'),(204,'Solomon Islands','sb','slb'),(205,'Somalia','so','som'),(206,'South Africa','za','zaf'),(207,'South Georgia and The South Sandwich Islands','gs',''),(208,'South Sudan','ss','ssd'),(209,'Spain','es','esp'),(210,'Sri Lanka','lk','lka'),(211,'Sudan','sd','sdn'),(212,'Suriname','sr','sur'),(213,'Svalbard and Jan Mayen','sj','sjm'),(214,'Swaziland','sz','swz'),(215,'Sweden','se','swe'),(216,'Switzerland','ch','che'),(217,'Syrian Arab Republic','sy','syr'),(218,'Taiwan, Province of China','tw',''),(219,'Tajikistan','tj','tjk'),(220,'Tanzania, United Republic of','tz','tza'),(221,'Thailand','th','tha'),(222,'Timor-Leste','tl','tls'),(223,'Togo','tg','tgo'),(224,'Tokelau','tk','tkl'),(225,'Tonga','to','ton'),(226,'Trinidad and Tobago','tt','tto'),(227,'Tunisia','tn','tun'),(228,'Turkey','tr','tur'),(229,'Turkmenistan','tm','tkm'),(230,'Turks and Caicos Islands','tc','tca'),(231,'Tuvalu','tv','tuv'),(232,'Uganda','ug','uga'),(233,'Ukraine','ua','ukr'),(234,'United Arab Emirates','ae','are'),(235,'United Kingdom','gb','gbr'),(236,'United States','us','usa'),(237,'United States Minor Outlying Islands','um',''),(238,'Uruguay','uy','ury'),(239,'Uzbekistan','uz','uzb'),(240,'Vanuatu','vu','vut'),(241,'Venezuela, Bolivarian Republic of','ve','ven'),(242,'Viet Nam','vn','vnm'),(243,'Virgin Islands, British','vg','vgb'),(244,'Virgin Islands, U.S.','vi','vir'),(245,'Wallis and Futuna','wf','wlf'),(246,'Western Sahara','eh','esh'),(247,'Yemen','ye','yem'),(248,'Zambia','zm','zmb'),(249,'Zimbabwe','zw','zwe');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `hash` varchar(200) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `language` varchar(2) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `customers`
--

LOCK TABLES `customers` WRITE;
/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
INSERT INTO `customers` VALUES (5,'alessandro.liguori@djherod.com','$2a$08$2izG20gCqPcfkMYoXpGT9eYzVV9YHjHZk5UWygO9r08K9v8cD9Vcq','4DdRxkzOWg7L5kGGLUM3gQ','2015-09-25 17:18:11','it',1);
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reviews` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `company_id` int(11) NOT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pros` text NOT NULL,
  `cons` text NOT NULL,
  `company` tinyint(4) NOT NULL,
  `salary` tinyint(4) NOT NULL,
  `management` tinyint(4) NOT NULL,
  `enviroment` tinyint(4) NOT NULL,
  `hr` tinyint(4) NOT NULL,
  `location` tinyint(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Reviews';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
INSERT INTO `reviews` VALUES (2,5,1,'2015-10-05 13:40:13','2015-10-05 15:57:00','Duis aliquam, metus et porttitor ultrices, sem urna iaculis nunc, eget lacinia metus mauris sit amet felis. Donec leo tortor, aliquam eu imperdiet a, dignissim id nisi. In in lobortis ex, vitae porta sapien. Nullam interdum justo eu mattis semper. Vestibulum vestibulum enim sit amet erat facilisis, nec gravida nulla dictum. Morbi rhoncus pharetra turpis ac luctus. Duis sed ultricies mauris, sed congue ante. Vestibulum quis viverra tellus, vitae pulvinar nulla. Ut lobortis eros vitae finibus tristique. Nullam vehicula eleifend consectetur. Aliquam sed metus tellus. Donec nec augue nec mi vestibulum mattis vitae et lorem. ','Duis aliquam egestas ultricies. Sed aliquam massa scelerisque, iaculis ligula ut, congue erat. Maecenas tellus lorem, volutpat et lacinia nec, accumsan eu sapien. Ut placerat ex massa, et accumsan velit ultricies vitae. Nullam sit amet ornare sapien, non condimentum purus. Vestibulum aliquet luctus congue. Suspendisse maximus dignissim erat non vulputate. Maecenas ultricies et velit ut vulputate. Praesent vitae velit nulla. Nulla pellentesque tristique ipsum at lobortis. Nulla ut est quis odio lobortis tincidunt. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. In ac libero aliquet, ultrices purus ut, auctor enim. ',1,2,3,4,5,5,1);
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-10-05 13:52:49
