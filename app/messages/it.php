<?php

// app/messages/it.php
$messages = array(
    //URL Translations - No special chars here
    "url_user"				    => "utente",
    "url_register"			    => "registrati",
    "url_login"                 => "entra",
    "url_logout"				=> "esci",
    "url_search"                => "cerca",
    "url_panel"                 => "pannello",
    "url_activate"              => "attiva",
    "url_reset"                 => "reset",
    "url_password"              => "password",
    "url_company"               => "societa",
    "url_reviews"               => "recensioni",
    "url_new"			        => "nuova",

    //Form Labels
    "label_company"             => "societ&agrave;",
    "label_email"               => "e-mail",
    "label_password"            => "password",
    "label_cfrm_password"       => "conferma password",
    "label_old_password"        => "password attuale",
    "label_company_name"        => "nome",
    "label_company_logo"        => "logo",
    "label_company_website"     => "sito internet",
    "label_company_country"     => "nazione",
    "label_company_city"        => "città",

    //Buttons
    "button_register"		    => "registrati",
    "button_login"			    => "entra",
    "button_search"			    => "cerca",
    "button_new_account"        => "registrati",
    "button_new_company"        => "invia",
    "button_newcompany"         => "nuova società",
    "button_login"              => "entra",
    "button_logout"             => "esci",
    "button_newreview"	        => "aggiungi recensione",

    //Error Messages
    "error_name_required"       => "Inserire il nome della societ&agrave;",
    "error_email_not_valid"	    => "Indirizzo email non valido",
    "error_email_required"	    => "Indirizzo email obbligatorio",
    "error_email_duplicate"     => "Indirizzo email &egrave; gi&agrave; registrato",
    "error_email_not_exist"     => "L'indirizzo e-mail non esiste",
    "error_password_required"   => "Password obbligatoria",
    "error_min_chars_pwd"	    => "La password deve contenere almeno 8 caratteri",
    "error_max_chars_pwd"	    => "La password pu&ograve; contenere massimo 20 caratteri",
    "error_pwd_match"		    => "Le due password non combiaciano",
    "error_user_already_active" => "L&apos;utente %account% &egrave; stato gi&agrave; attivato",
    "error_user_not_exist"      => "L&apos;utente %account% non esiste",
    "error_user_not_correct"    => "Utente o password errati",
    "error_user_not_logged"     => "Accedere con il proprio utente e password per visualizzare la pagina",
    "error_password_wrong"      => "La password inserita non è corretta",
    "error_companyname_required"=> "Il nome della società è obbligatorio",
    "error_companyurl_wrong"    => "L'indirizzo internet non è corretto",
    "error_companycountry_req"  => "Il campo nazione è obbligatorio",
    "error_file_format_wrong"   => "Il formato del file è errato, inserire solamente files jpg o png",
    "error_url_format_wrong"    => "L'indirizzo internet è errato.",
    "error_company_not_correct" => "La società selezionata non esiste",
    "error_review_already_wrote"=> "Il commento è già presente nel database, non è possibile inserire due commenti per la stessa società",
    "error_text_too_short"      => "Testo troppo corto, per favore inserisca maggiori informazioni",
    "error_mandatory_field"     => "Questo campo è obbligatorio",
    
    //Email Texts
    "textemail_confirm_text"    => "Conferma il tuo indirizzo e-mail cliccando sul link in basso.",
    "textemail_confirm_button"  => "Conferma l'indirizzo e-mail",
    "textemail_confirm_subject" => "Should I work in? - Conferma l'indirizzo e-mail",
    "textemail_reset_text"      => "Cambia la password cliccando sul link in basso.",
    "textemail_reset_button"    => "Cambia password",
    "textemail_reset_subject"   => "Should I work in? - Cambia Password",

    //Short Texts
    "user_activated"            => "Il suo account %account% &egrave; stato attivato",
    "user_created"              => "Il suo account &egrave; stato creato, ricever&agrave; a breve una e-mail di attivazione",
    "email_foget_password_sent" => "Le abbiamo inviato una email con la procedura per cambiare la sua password",
    "password_updated"          => "La sua password è stata aggiornata",
    "company_created"           => "Il profilo della società è stato creato correttamente, la società apparirà nel sistema nelle prossime 24 ore.",
    "review_created"            => "Il commento è stato inserito correttamente, il commento apparirà nel sistema nelle prossime 24 ore.",

    //Long Texts
);
