<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;

class SearchcompanyForm extends Form
{

    public function initialize($entity = null, $options = array())
    {

        $name = new Text("company");
        $name->setLabel("label_company");
        $name->setFilters(array('striptags', 'string'));
        $name->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_name_required'
            ))
        ));
        $this->add($name);
    }
}