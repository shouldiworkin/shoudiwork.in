<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\File;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Url as UrlValidator;

class CompanyForm extends Form
{

    public function initialize($entity = null, $options = array())
    {

        $email = new Text("name");
        $email->setLabel("label_company_name");
        $email->setFilters(array('striptags', 'string'));
        $email->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_companyname_required'
            ))
        ));
        $this->add($email);

        $logo = new File("logo");
        $logo->setLabel("label_company_logo");
        $this->add($logo);


        $website = new Text("website");
        $website->setLabel("label_company_website");
        $website->setFilters(array('striptags', 'string'));
        $website->addValidators(array(
            new UrlValidator(array(
               'message' => 'error_url_format_wrong',
               'allowEmpty' => true,
            ))
        ));
        $this->add($website);

        $country = new Select("country",Countries::find(),
                array('using' => array('alpha_2','name')
            ));
        $country->setLabel("label_company_country");
        $country->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_companycountry_req'
            ))
        ));
        $this->add($country);

        $city = new Text("city");
        $city->setLabel("label_company_city");
        $city->setFilters(array('striptags', 'string'));
        $this->add($city);
    }
}