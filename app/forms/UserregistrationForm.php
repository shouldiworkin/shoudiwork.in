<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;
use Phalcon\Validation\Validator\StringLength;
use Phalcon\Validation\Validator\Confirmation;

class UserregistrationForm extends Form
{

    public function initialize($entity = null, $options = array())
    {

        $email = new Text("email");
        $email->setLabel("label_email");
        $email->setFilters(array('striptags', 'string'));
        $email->addValidators(array(
            new Email(array(
                'message' => 'error_email_not_valid'
            )),
            new PresenceOf(array(
                'message' => 'error_email_required'
            ))
        ));
        $this->add($email);

        $password = new Password("password");
        $password->setLabel("label_password");
        $password->setFilters(array('striptags', 'string'));
        $password->addValidators(array(
            new StringLength(array(
                'max' => 20,
                'min' => 8,
                'messageMaximum' => 'error_max_chars_pwd',
                'messageMinimum' => 'error_min_chars_pwd'
            )),
            new Confirmation(array(
               'message' => 'error_pwd_match',
               'with' => 'confirm_password'
            ))
        ));
        $this->add($password);

        $confirm_password = new Password("confirm_password");
        $confirm_password->setLabel("label_cfrm_password");
        $confirm_password->setFilters(array('striptags', 'string'));
        $confirm_password->addValidators(array(
            new StringLength(array(
                'max' => 20,
                'min' => 8,
                'messageMaximum' => 'error_max_chars_pwd',
                'messageMinimum' => 'error_min_chars_pwd'
            ))
        ));
        $this->add($confirm_password);
    }
}