<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class ForgotpasswordForm extends Form
{

    public function initialize($entity = null, $options = array())
    {


        $email = new Text("email");
        $email->setLabel("label_email");
        $email->setFilters(array('striptags', 'string'));
        $email->addValidators(array(
            new Email(array(
                'message' => 'error_email_not_valid'
            )),
            new PresenceOf(array(
                'message' => 'error_email_required'
            ))
        ));
        $this->add($email);
    }
}