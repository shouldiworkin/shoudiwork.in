<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Element\Password;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\Email;

class LoginForm extends Form
{

    public function initialize($entity = null, $options = array())
    {

        $email = new Text("email");
        $email->setLabel("label_email");
        $email->setFilters(array('striptags', 'string'));
        $email->addValidators(array(
            new Email(array(
                'message' => 'error_email_not_valid'
            )),
            new PresenceOf(array(
                'message' => 'error_email_required'
            ))
        ));
        $this->add($email);

        $password = new Password("password");
        $password->setLabel("label_password");
        $password->setFilters(array('striptags', 'string'));
        $password->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_password_required'
            ))
        ));
        $this->add($password);
    }
}