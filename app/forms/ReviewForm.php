<?php

use Phalcon\Forms\Form;
use Phalcon\Forms\Element\Textarea;
use Phalcon\Forms\Element\Select;
use Phalcon\Validation\Validator\PresenceOf;
use Phalcon\Validation\Validator\StringLength;

class ReviewForm extends Form
{

    public function initialize($entity = null, $options = array())
    {

        $ratings=array(1=>1,2=>2,3=>3,4=>4,5=>5);

        $company = new Select("company",$ratings);
        $company->setLabel("label_review_company");
        $company->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($company);

        $salary = new Select("salary",$ratings);
        $salary->setLabel("label_review_salary");
        $salary->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($salary);

        $management = new Select("management",$ratings);
        $management->setLabel("label_review_management");
        $management->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($management);

        $enviroment = new Select("enviroment",$ratings);
        $enviroment->setLabel("label_review_enviroment");
        $enviroment->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($enviroment);

        $hr = new Select("hr",$ratings);
        $hr->setLabel("label_review_hr");
        $hr->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($hr);

        $location = new Select("location",$ratings);
        $location->setLabel("label_review_location");
        $location->addValidators(array(
            new PresenceOf(array(
                'message' => 'error_mandatory_field'
            ))
        ));
        $this->add($location);

        $pros = new Textarea("pros");
        $pros->setLabel("label_review_pros");
        $pros->setFilters(array('striptags', 'string'));
        $pros->addValidators(array(
            new StringLength(array(
              'min' => 10,
              'messageMinimum' => 'error_text_too_short'
            )),
        ));
        $this->add($pros);

        $cons = new Textarea("cons");
        $cons->setLabel("label_review_cons");
        $cons->setFilters(array('striptags', 'string'));
        $cons->addValidators(array(
            new StringLength(array(
              'min' => 10,
              'messageMinimum' => 'error_text_too_short'
            )),
        ));
        $this->add($cons);
    }
}