<?php

use Phalcon\Mvc\Model;

class Reviews extends Model
{
  public $id;
  public $author_id;
  public $company_id;
  public $created;
  public $pros;
  public $cons;
  public $company;
  public $salary;
  public $management;
  public $enviroment;
  public $hr;
  public $location;
  public $active;

  public function initialize(){
    $this->belongsTo("company_id", "Companies", "id");
    $this->belongsTo("author_id", "Customers", "id");
  }
}