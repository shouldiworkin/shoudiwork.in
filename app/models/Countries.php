<?php

use Phalcon\Mvc\Model;

class Countries extends Model
{
    public $id;
    public $name;
    public $alpha_2;
    public $alpha_3;

    public function initialize(){
    	$this->belongsTo("alpha_2", "Companies", "country");
    }
}