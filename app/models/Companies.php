<?php

use Phalcon\Mvc\Model;

class Companies extends Model
{
    public $id;
    public $name;
    public $slug;
    public $logo;
    public $website;
    public $country;
    public $city;
    public $created;
    public $company_ratio;
    public $salary_ratio;
    public $management_ratio;
    public $enviroment_ratio;
    public $hr_ratio;
    public $location_ratio;
    public $comments_qty;
    public $created_by;
    public $active;

    public function beforeValidationOnCreate(){
        $this->created = date('Y-m-d H:i:s');
        $this->active = 0;
    }

    public function initialize(){
        $this->hasMany('id', 'Reviews', 'company_id');
        $this->hasOne('country', 'Countries', 'alpha_2');
        $this->hasOne('created_by', 'Customers', 'id');

        $this->skipAttributesOnCreate(
            array('company_ratio',
                'salary_ratio',
                'management_ratio',
                'enviroment_ratio',
                'hr_ratio',
                'location_ratio',
                'comments_qty',
                )
            );

        $this->allowEmptyStringValues(
            array('city',
                'website',
                'logo',
                )
            );
    }

    public function getReviews(){
        $parameters=array("conditions" => "active=1");
        return $this->getRelated('Reviews', $parameters);
    }

}