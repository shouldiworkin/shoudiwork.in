<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Validator\Uniqueness;

class Customers extends Model
{
    public $id;
    public $email;
    public $password;
    public $hash;
    public $created;
    public $language;
    public $active;

    public function initialize(){
        $this->hasMany('id', 'Reviews', 'author_id');
    }

    public function validation()
    {
        $this->validate(new Uniqueness(array(
            "field"   => "email",
            "message" => "error_email_duplicate"
            ))
        );
        if ($this->validationHasFailed() == true) {
            return false;
        }
    }
}