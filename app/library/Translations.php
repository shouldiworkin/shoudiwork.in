<?php

use Phalcon\Mvc\User\Component;
use Phalcon\Translate\Adapter\NativeArray;

/**
 * Translations
 *
 * Helps to translate the strings everywhere
 */
class Translations extends Component
{
	private $translate;
	private $url_language;
	private $default_language='en';

	public function setTranslate(){
		$url_language=$this->dispatcher->getParam('language');
		$query=$this->request->getServer('REQUEST_URI');
		
		//In case of images
		if (substr($query,1,5)=='image'){
			$url_language=$this->default_language;
		}

		//In case of wrong url try to get the languange
		if (empty($url_language)){
        	
        	if (strlen($query)>3){
        		$url_language=substr($query, 1,2);
        		header('location: /'.$url_language.'/404');
        		exit();
        	} else {
        		header('location: /'.$this->default_language);
        	}
        }

        if (file_exists("../app/messages/" . $url_language . ".php")){
            require "../app/messages/" . $url_language . ".php";
            $this->url_language=$url_language;
        } else {
            $language = substr($this->request->getBestLanguage(),0,2);
            if (file_exists("../app/messages/" . $language . ".php")){
                $this->response->redirect('/'.$language);
            } else {
                $this->response->redirect('/'.$this->default_language);
            }

        }

		$this->translate = new NativeArray(
            array(
                "content" => $messages
            )
        );
	}

	public function _($string,$data=array()){
		if (empty($data)){
			return $this->translate->_($string);
		} else {
			return $this->translate->_($string,$data);
		}
	}

	public function getLanguage(){
		return $this->url_language;
	}

	public function getUrl($data=array()){
		$url='/'.$this->getLanguage();
		foreach ($data as $value) {
			if (substr($value, 0,4)=='url_'){
				$url.='/'.$this->translate->_($value);
			} else {
				$url.='/'.$value;
			}
		}
		return $url;
	}

}