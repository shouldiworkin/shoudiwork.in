<?php

/**
 * Utilities
 *
 * Library of simple functions
 */
class Utilities
{
	/**
     * Creates a slug to be used for pretty URLs.
     *
     * @link http://cubiq.org/the-perfect-php-clean-url-generator
     * @author Andres Gutierrez <andres@phalconphp.com>, Nikolaos Dimopoulos <nikos@niden.net>, Ilgıt Yıldırım <ilgityildirim@gmail.com>, Alessandro Liguori
     * @param                     $string
     * @param  array              $replace
     * @param  string             $delimiter
     * @return mixed
     * @throws \Phalcon\Exception
     */
	public function slug($string, $replace = array("'"), $delimiter = '-')
    {
        if (!extension_loaded('iconv')) {
            throw new \Phalcon\Exception('iconv module not loaded');
        }
        $string = transliterator_transliterate("Any-Latin; Latin-ASCII; Lower();", $string);
        // Save the old locale and set the new locale to UTF-8
        $oldLocale = setlocale(LC_ALL, '0');
        setlocale(LC_ALL, 'en_US.UTF-8');
        // Better to replace given $replace array as index => value
        // Example $replace['ı' => 'i', 'İ' => 'i'];
        if (!empty($replace) && is_array($replace)) {
            $string = str_replace(array_keys($replace), array_values($replace), $string);
        }
        // replace non letter or non digits by -
        $string = preg_replace("#[^\\pL\d]+#u", '-', $string);
        // Trim trailing -
        $string = trim($string, '-');
        $clean = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
        $clean = strtolower($clean);
        $clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);
        $clean = trim($clean, $delimiter);
        // Revert back to the old locale
        setlocale(LC_ALL, $oldLocale);
        return $clean;
    }

    public function randomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}