<?php

/**
 * Emails
 *
 * Send Emails trough Mailgun
 */
class Mails
{
  protected $key;
  protected $domain;
  protected $from;

  protected function br2nl($string){
    return preg_replace('/\<br(\s*)?\/?\>/i', "\n", $string);
  }

  protected function getLinks($html){
    $urls = array();
    $dom = new DomDocument();
    $dom->loadHTML($html);
    foreach ($dom->getElementsByTagName('a') as $node) {
      $urls[]=$node->getAttribute( 'href' );
    }
    return $urls;
  }

  public function setApiKey($key){
    $this->key = $key;
  }

  public function setDomain($domain){
    $this->domain = $domain; 
  }

  public function setFrom($from){
    $this->from = $from; 
  }

  public function send($to, $subject, $message) {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, 'api:'.$this->key);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

    //Convert to plain text
    $urls = $this->getLinks($message);
    $plain=preg_replace("/<style\\b[^>]*>(.*?)<\\/style>/s", "", $message);
    $plain=preg_replace('/\t+/', '', $plain);
    $plain = strip_tags($this->br2nl($plain));
    foreach ($urls as $url) {
      $plain .= "\r\n".$url;
    }

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
    curl_setopt($ch, CURLOPT_URL, 'https://api.mailgun.net/v3/'.$this->domain.'/messages');
    curl_setopt($ch, CURLOPT_POSTFIELDS, array('from' => $this->from,
          'to' => $to,
          'subject' => $subject,
          'html' => $message,
          'text' => $plain));

    $json = json_decode(curl_exec($ch));

    $info = curl_getinfo($ch);

    curl_close($ch);

    return $json;
  }
}