<?php

use Phalcon\Mvc\View;
use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Dispatcher;
use Phalcon\Mvc\Url as UrlProvider;
use Phalcon\Mvc\Model\Metadata\Memory as MetaData;
use Phalcon\Session\Adapter\Files as Session;
use Phalcon\Flash\Session as FlashSession;
use Phalcon\Events\Manager as EventsManager;
use Phalcon\Translate\Adapter\NativeArray;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

$di->set('dispatcher', function () use ($di){
	$eventsManager = new EventsManager;
	$eventsManager->attach('dispatch:beforeDispatch', new SecPlugin);
	$eventsManager->attach('dispatch:beforeException', new NotFoundPlugin);
	$dispatcher = new Dispatcher;
	$dispatcher->setEventsManager($eventsManager);

	return $dispatcher;
});

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
	$url = new UrlProvider();
	$url->setBaseUri($config->application->baseUri);
	return $url;
});


$di->set('view', function () use ($config) {

	$view = new View();

	$view->setViewsDir(APP_PATH . $config->application->viewsDir);

	$view->registerEngines(array(
	    ".phtml" => function($view, $di) {
			$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
			$volt->setOptions(array(
				"compiledPath" => "../app/cache/templates/"
			));
			return $volt;
		}
	));

	return $view;
});


/**
 * Database connection
 */
$di->set('db', function () use ($config) {
	$dbclass = 'Phalcon\Db\Adapter\Pdo\\' . $config->database->adapter;
	return new $dbclass(array(
		"host"     => $config->database->host,
		"username" => $config->database->username,
		"password" => $config->database->password,
		"dbname"   => $config->database->name
	));
});

/**
 * Start the session
 */
$di->set('session', function () {
    $session = new Session();
    $session->start();
    return $session;
});

/**
 * Register a user component
 */
$di->set('translations', function () {
	$obj = new Translations;
	$obj->setTranslate();
	return $obj;
});

/**
 * Register Mail component (MailGun)
 */
$di->set('mail', function () use ($config) {
	$mail = new Mails;
	$mail->setDomain($config->email->domain);
	$mail->setApiKey($config->email->apikey);
	$mail->setFrom($config->email->from);
	return $mail;
});

/**
 * Register simple template compiler (emails templates)
 */
$di->set('simpleView', function() use ($config) {
    $view = new Phalcon\Mvc\View\Simple();
    $view->setViewsDir(APP_PATH.$config->application->viewsDir);
    $view->registerEngines(array(
	    ".phtml" => function($view, $di) {
			$volt = new \Phalcon\Mvc\View\Engine\Volt($view, $di);
			$volt->setOptions(array(
				"compiledPath" => "../app/cache/emails/"
			));
			return $volt;
		}
	));
    return $view;
});

/**
 * Register configuration
 */
$di->set('config', $config);