<?php
use Phalcon\Mvc\Router;

$di->set('router', function(){
	//EN, ES, IT, FR
	$addresses=array(
		'user'=>array('user','usuario','utente','utilisateur'),
		'search'=>array('search','buscar','cerca','recherche'),
		'login'=>array('login','entrar','entra','entrer'),
		'logout'=>array('logout','salir','esci','deconnecter'),
		'signup'=>array('signup','alta','registrati','inscrit'),
		'activate'=>array('activate','activar','attiva','activer'),
		'forgot'=>array('forgot','olvidada','dimenticata','oublie'),
		'reset'=>array('reset'),
		'panel'=>array('panel','panel','pannello','panneau'),
		'password' =>array('password'),
		'company' =>array('company','empresa','societa','societe'),
		'new' =>array('new','nueva','nuova','nouvelle'),
		'reviews' =>array('reviews','opiniones','recensioni','commentaires'),
	);

	$router = new Router();

	$router->add("/{language:[a-z]{2}}", array(
	    'controller' => 'index',
    	'action' => 'index'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['search']).")}/{company}", array(
	    'controller' => 'index',
    	'action' => 'search'
	));

	//Errors Controller
	$router->add("/{language:[a-z]{2}}/404", array(
	    'controller' => 'errors',
    	'action' => 'show404'
	));

	//Company Controller
	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['company']).")}/{pageSlug:(".implode('|', $addresses['new']).")}", array(
	    'controller' => 'company',
    	'action' => 'new'
	));

	//Reviews Controller
	$router->add("/{language:[a-z]{2}}/{company}/{pageSlug:(".implode('|', $addresses['reviews']).")}/{pageSlug:(".implode('|', $addresses['new']).")}", array(
	    'controller' => 'review',
    	'action' => 'new'
	));

	$router->add("/{language:[a-z]{2}}/{company}/{pageSlug:(".implode('|', $addresses['reviews']).")}", array(
	    'controller' => 'review',
    	'action' => 'list'
	));

	//User Controller
	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['login']).")}", array(
	    'controller' => 'user',
    	'action' => 'login'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['logout']).")}", array(
	    'controller' => 'user',
    	'action' => 'logout'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['signup']).")}", array(
	    'controller' => 'user',
    	'action' => 'register'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['activate']).")}/{email}/{hashcode}", array(
	    'controller' => 'user',
    	'action' => 'activate'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['password']).")}/{pageSlug:(".implode('|', $addresses['forgot']).")}", array(
	    'controller' => 'user',
    	'action' => 'forgetpassword'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['password']).")}/{pageSlug:(".implode('|', $addresses['reset']).")}/{email}/{hashcode}", array(
	    'controller' => 'user',
    	'action' => 'resetpassword'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['panel']).")}", array(
	    'controller' => 'user',
    	'action' => 'panel'
	));

	$router->add("/{language:[a-z]{2}}/{pageSlug:(".implode('|', $addresses['user']).")}/{pageSlug:(".implode('|', $addresses['password']).")}", array(
	    'controller' => 'user',
    	'action' => 'password'
	));

	//Image Controller
	$router->add("/image/{folder}/{dimensions}/{file}", array(
	    'controller' => 'image',
    	'action' => 'load'
	));
	
	return $router;
});
