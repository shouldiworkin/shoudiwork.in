<?php

use Phalcon\Image\Adapter\Imagick;

class ImageController extends ControllerBase
{
    public function loadAction()
    {
        $folder=$this->dispatcher->getParam('folder');
        $dimensions=$this->dispatcher->getParam('dimensions');
        $dimensions_values=explode('x',$dimensions);
        $file=$this->dispatcher->getParam('file');
        $path_destination=APP_PATH.$this->config->application->cacheDir.'images/'.$folder.'/'.$dimensions;
        $path_original=APP_PATH.$this->config->application->imagesDir.$folder;
        if (file_exists($path_original.'/'.$file)){
            if (!file_exists($path_destination.'/'.$file)) {
                if (!file_exists($path_destination)){
                    mkdir($path_destination, 0755, true);
                }
                $image = new Imagick($path_original.'/'.$file);
                $image->resize($dimensions_values[0],$dimensions_values[1]);
                $image->save($path_destination.'/'.$file,80);
            }
            
            //Fix this, use Phalcon setHeader
            $image = new Imagick($path_destination.'/'.$file);
            header("Content-Type: ".$image->getMime());
            header("Expires: 0");
            header("Content-Length: ".filesize($path_destination.'/'.$file));
            readfile($path_destination.'/'.$file);
            die();
        } else {
            $this->response->redirect($this->translations->getUrl());
        }
    }   
}
