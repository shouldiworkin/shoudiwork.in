<?php

class UserController extends ControllerBase
{
    public function loginAction()
    {
        $this->translations;
        $session=$this->session->get('auth-identity');
        if (!empty($session['email'])){
            return $this->response->redirect($this->translations->getUrl(array('url_user','url_panel')));
        }

        $form = new LoginForm();

        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                if ($this->security->checkToken()) {
                    $email      = $this->request->getPost('email');
                    $password   = $this->request->getPost('password');

                    $customer = Customers::findFirst(array(
                        "email = :email: AND active = '1'",
                        'bind' => array('email' => $email)
                    ));

                    if (!empty($customer->email)){
                        if ($this->security->checkHash($password, $customer->password)) {

                            $this->session->set('auth-identity', array(
                                'id' => $customer->id,
                                'email' => $customer->email
                            ));
                            return $this->response->redirect($this->translations->getUrl(array('url_user','url_panel')));
                        }         
                    } 
                }
                $this->flashSession->error($this->translations->_('error_user_not_correct'));
            }
        }

        $this->view->form = $form;
    }

    public function logoutAction()
    {
        $this->session->destroy('auth-identity');
        return $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
    }

    public function registerAction()
    {
        $form = new UserregistrationForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                if ($this->security->checkToken()) {
                    $customer = new Customers();
                    $customer->email=$this->request->getPost('email');
                    $customer->password=$this->security->hash($this->request->getPost('password'));
                    $hash = $this->security->getSaltBytes();
                    $customer->hash=$hash;
                    $customer->created=date('Y-m-d H:i:s');
                    $customer->language=$this->translations->getLanguage();
                    $customer->active=0;
                    if ($customer->save() == false) {
                        foreach ($customer->getMessages() as $message) {
                            $this->flashSession->error($this->translations->_($message->getMessage()));
                        }
                    } else {
                        $this->flashSession->success($this->translations->_('user_created'));
                        $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
                        $activate_link=$this->url->get($this->translations->getUrl(array('url_user','url_activate',urlencode($this->request->getPost('email')),urlencode($hash))));
                        $messagehtml = $this->simpleView->render("emails/confirm", array('confirm_text'=>$this->translations->_('textemail_please_confirm'),
                                                                         'confirm_button'=>$this->translations->_('textemail_confirm_button'),
                                                                         'confirm_link'=>$activate_link,
                                                                         'confirm_title'=>$this->translations->_('textemail_confirm_subject'),
                                                                         ));
                        $this->mail->send($this->request->getPost('email'),$this->translations->_('textemail_confirm_subject'),$messagehtml);
                    }
                }
            }
        }

        $this->view->form = $form;
    }

    public function activateAction()
    {
        $email=$this->dispatcher->getParam('email');
        $hashcode=$this->dispatcher->getParam('hashcode');
        $customer = Customers::findFirst(array(
            "email = :email: AND hash = :hashcode:",
            'bind' => array('email' => $email, 'hashcode' => $hashcode),
            'bindTypes' => array('email' => Phalcon\Db\Column::BIND_PARAM_STR,'hashcode'=>Phalcon\Db\Column::BIND_PARAM_STR)
        ));
        if (!empty($customer->id)){
            if ($customer->active==1){
                $message='error_user_already_active';
            } else {
                $customer->active=1;
                $customer->save();
                $message='user_activated';
            }
        } else {
            $message='error_user_not_exist';
        }

        $this->view->message = $message;
        $this->view->email = $email;
    }

    public function passwordAction(){
        $form = new ResetpasswordForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                if ($this->security->checkToken()) {
                    $password = $this->request->getPost('password');
                    $old_password = $this->request->getPost('oldpassword');


                    $session=$this->session->get('auth-identity');
                    $customer = Customers::findFirst(array(
                        "email = :email: AND id = :id:",
                        'bind' => array('email' => $session['email'], 'id' => $session['id']),
                    ));

                    if ($this->security->checkHash($old_password, $customer->password)) {
                        $customer->password=$this->security->hash($password);
                        $customer->save();
                        $this->flashSession->success($this->translations->_('password_updated'));

                        $this->session->destroy('auth-identity');
                        return $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
                    } else {
                        $this->flashSession->error($this->translations->_('error_password_wrong'));
                    }
                }
            }
        }
        $this->view->form = $form;
    }

    public function forgetpasswordAction(){
        $form = new ForgotpasswordForm();
        if ($this->request->isPost()) {
            if ($form->isValid($this->request->getPost())) {
                if ($this->security->checkToken()) {
                    $email    = $this->request->getPost('email');
                    $customer = Customers::findFirst(array(
                        "email = :email:",
                        'bind' => array('email' => $email)
                    ));
                    if (!empty($customer->id)){
                        $hash=$this->security->getSaltBytes();
                        $customer->hash=$hash;
                        $customer->save();
                        
                        $resetpassword_link=$this->url->get($this->translations->getUrl(array('url_user','url_password','url_reset',urlencode($this->request->getPost('email')),urlencode($hash))));
                        $messagehtml = $this->simpleView->render("emails/resetpassword", array('reset_text'=>$this->translations->_('textemail_reset_text'),
                                                                         'reset_button'=>$this->translations->_('textemail_reset_button'),
                                                                         'reset_link'=>$resetpassword_link,
                                                                         'reset_title'=>$this->translations->_('textemail_reset_subject'),
                                                                         ));
                        $this->mail->send($this->request->getPost('email'),$this->translations->_('textemail_reset_subject'),$messagehtml);
                        $this->flashSession->success($this->translations->_('email_foget_password_sent'));

                        $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
                    } else {
                        $this->flashSession->error($this->translations->_('error_email_not_exist'));
                    }
                }
            }
        }
        $this->view->form = $form;
    }

    public function resetpasswordAction(){
        $email=$this->dispatcher->getParam('email');
        $hashcode=$this->dispatcher->getParam('hashcode');
        $customer = Customers::findFirst(array(
            "email = :email: AND hash = :hashcode:",
            'bind' => array('email' => $email, 'hashcode' => $hashcode),
            'bindTypes' => array('email' => Phalcon\Db\Column::BIND_PARAM_STR,'hashcode'=>Phalcon\Db\Column::BIND_PARAM_STR)
        ));

        if (!empty($customer->id)){
            $form = new PublicresetpasswordForm();
            if ($this->request->isPost()) {
                if ($form->isValid($this->request->getPost())) {
                    if ($this->security->checkToken()) {
                        $customer->password=$this->security->hash($this->request->getPost('password'));
                        $customer->save();
                        $this->flashSession->success($this->translations->_('password_updated'));
                        return $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
                    }
                }
            }
            $this->view->form = $form;
        } else {
            $this->flashSession->error($this->translations->_('error_email_not_exist'));
            return $this->response->redirect($this->translations->getUrl(array('url_user','url_login')));
        }
        
    }

    public function panelAction(){
        
    }
}
