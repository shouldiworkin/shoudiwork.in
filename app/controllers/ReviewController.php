<?php

class ReviewController extends ControllerBase
{
	public function newAction(){
        $company_slug = $this->dispatcher->getParam('company');
        $company = Companies::findFirst(array(
            "slug = :slug:",
            'bind' => array('slug' => $company_slug),
            'bindTypes' => array('slug' => Phalcon\Db\Column::BIND_PARAM_STR)
        ));

        $session=$this->session->get('auth-identity');
        if ($company->active==1 || $company->created_by==$session['id']){
            $review = Reviews::findFirst(array(
                "author_id = :author_id: AND company_id = :company_id:",
                'bind' => array('author_id' => $session['id'],'company_id'=>$company->id)
            ));
            if ($review==false){
                $form = new ReviewForm();
                if ($this->request->isPost()) {
                    if ($form->isValid($this->request->getPost())) {
                        if ($this->security->checkToken()) {
                            $review = new Reviews();
                            $review->assign($this->request->getPost());
                            $review->author_id=$session['id'];
                            $review->company_id=$company->id;
                            $review->created=date('Y-m-d H:i:s');
                            $review->active=0;
                            $review->save();
                            $this->flashSession->success($this->translations->_('review_created'));
                            return $this->response->redirect($this->translations->getUrl(array('url_user','url_panel')));
                        }
                    }
                }
                $this->view->form = $form;
            } else {
                $this->flashSession->error($this->translations->_('error_review_already_wrote'));
                return $this->response->redirect($this->translations->getUrl(array('url_user','url_panel')));
            }
        } else {
            $this->flashSession->error($this->translations->_('error_company_not_correct'));
            return $this->response->redirect($this->translations->getUrl(array('url_user','url_panel')));
        }
	}

    public function listAction(){
        $company_slug = $this->dispatcher->getParam('company');
        $company = Companies::findFirst(array(
            "slug = :slug: AND active = 1",
            'bind' => array('slug' => $company_slug),
            'bindTypes' => array('slug' => Phalcon\Db\Column::BIND_PARAM_STR)
        ));
        if ($company != false){
            $this->view->company=$company;
        } else {
            return $this->response->redirect($this->translations->getUrl());
        }

    }
}