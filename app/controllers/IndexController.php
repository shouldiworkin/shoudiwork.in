<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
        $form = new SearchcompanyForm();

        if ($this->request->isPost()) {

            if (!$form->isValid($this->request->getPost())) {
                foreach ($form->getMessages() as $message) {
                    $this->view->message = $message;
                }
            } else {
                $company=$this->request->getPost("company");
                $this->response->redirect($this->translations->getUrl(array('url_search',urlencode($company))));
            }
        }

        $this->view->form = $form;
    }

    public function searchAction()
    {
        $company = $this->dispatcher->getParam('company');
        $params = array(
                "name LIKE :name: OR slug LIKE :name: AND active=1",
                'bind' => array('name' => '%' .$company. '%'),
                'bindTypes' => array('name' => Phalcon\Db\Column::BIND_PARAM_STR)
            );
        $companies = Companies::find($params);
        
        $this->view->company = $company;
        $this->view->companies = $companies;
    }
}
