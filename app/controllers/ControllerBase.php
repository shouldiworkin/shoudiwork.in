<?php

use Phalcon\Mvc\Controller;
use Phalcon\Translate\Adapter\NativeArray;

class ControllerBase extends Controller
{
    public function initialize()
    {
        $session=$this->session->get('auth-identity');
        if (!empty($session)){
        	$this->view->session = $session;	
        }
        $this->view->translations = $this->translations;
        $this->view->url_language = $this->translations->getLanguage();
        $this->view->backurl = urlencode($this->request->getServer('REQUEST_URI'));
    }
}