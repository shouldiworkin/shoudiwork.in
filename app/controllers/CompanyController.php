<?php

class CompanyController extends ControllerBase
{
	protected function getCompanySlug($id,$country,$name){
		return strtolower(Utilities::slug($name).'-'.$country.'-'.$id);
	}

	public function newAction(){
        $form = new CompanyForm();
        $country_code=geoip_record_by_name($this->request->getClientAddress());
        if (!empty($country_code['country_code'])){
            $form->get('country')->setDefault(strtolower($country_code['country_code']));
        }

        if ($this->request->isPost()) {
            $check = true;
            if ($form->isValid($this->request->getPost())) {
                if ($this->security->checkToken()) {
                    $gen_id=strtolower(Utilities::randomString(3));
                    $slug=$this->getCompanySlug($gen_id,$this->request->getPost('country'),$this->request->getPost('name'));
                    
                    if($this->request->hasFiles()){
                        $file=$this->request->getUploadedFiles()[0];
                        if (!empty($file->getName())){
                            $path_parts =pathinfo($file->getName()); 
                            $logo='logo-'.$slug.'.'.strtolower($path_parts['extension']);
                            if (in_array($file->getRealType(), array('image/png','image/jpg','image/jpeg'))){
                                $file->moveTo('../'.$this->config->application->imagesDir.'logos/'.$logo);  
                            } else {
                                $check = false;
                                $this->flashSession->error($this->translations->_('error_file_format_wrong'));
                            }
                        }
                    }

                    if ($check){
                        $empty = new Phalcon\Db\RawValue('');
                        $company = new Companies();
                        $company->assign($this->request->getPost());
                        $company->slug=$slug;
                        if (!empty($logo)){
                            $company->logo = $logo;
                        } else {
                            $company->logo = '';
                        }
                        $company->city=$this->request->getPost('city');
                        $company->website=$this->request->getPost('website');
                        $company->created_by=$this->session->get('auth-identity')['id'];
                        $company->save();
                        $this->flashSession->success($this->translations->_('company_created'));
                        //Put inside the admin the check for the url_
                        return $this->response->redirect($this->translations->getUrl(array($slug,'url_reviews','url_new')));
                    }
                }
            }
        }
        
        $this->view->form = $form;
	}
}
